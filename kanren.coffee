_ = require "underscore"

# Nondeterministic functions
fail = exports.fail = (x) ->
  []

succeed = exports.succeed = (x) ->
  [x]

_disj = (f1, f2) ->
  (x) ->
    [].concat f1(x), f2(x)

disj = exports.disj = ->
  fs = _.toArray(arguments)
  return fail  if _.isEmpty(fs)
  _disj _.first(fs), disj.apply(this, _.rest(fs))

_conj = (f1, f2) ->
  (x) ->
    _.flatten _.map(f1(x), f2), true

conj = exports.conj = ->
  fs = _.toArray(arguments)
  return succeed  if _.isEmpty(fs)
  return _.first(fs)  if fs.length is 1
  _conj _.first(fs), (s) ->
    conj.apply(null, _.rest(fs)) s



# Logic variables
lvar = exports.lvar = (name) ->
  "_." + name

is_lvar = exports.is_lvar = (v) ->
  typeof v is "string" and v.slice(0, 2) is "_."

empty_subst = exports.empty_subst = {}
ext_s = exports.ext_s = (variable, value, s) ->
  n = {}
  n[variable] = value
  _.extend {}, s, n

lookup = exports.lookup = (variable, s) ->
  return variable  unless is_lvar(variable)
  return lookup(s[variable], s)  if s.hasOwnProperty(variable)
  variable

unify = exports.unify = (t1, t2, s) ->
  t1 = lookup(t1, s)
  t2 = lookup(t2, s)
  return s  if _.isEqual(t1, t2)
  return ext_s(t1, t2, s)  if is_lvar(t1)
  return ext_s(t2, t1, s)  if is_lvar(t2)
  if _.isArray(t1) and _.isArray(t2)
    s = unify(_.first(t1), _.first(t2), s)
    s = unify(_.rest(t1), _.rest(t2), s)  if s isnt null
    return s
  null

vx = exports.vx = lvar("x")
vy = exports.vy = lvar("y")
vz = exports.vz = lvar("z")
vq = exports.vq = lvar("q")

# Logic engine
eq = exports.eq = (t1, t2) ->
  (s) ->
    r = unify(t1, t2, s)
    if r isnt null
      succeed r
    else
      fail s

membero = exports.membero = (variable, list) ->
  if _.isEmpty(list)
    fail
  else
    disj eq(variable, _.first(list)), membero(variable, _.rest(list))

joino = exports.joino = (a, b, l) ->
  eq [
    a
    b
  ], l

_lookup = (variable, s) ->
  v = lookup(variable, s)
  return v  if is_lvar(v)
  if _.isArray(v)
    if _.isEmpty(v)
      return v
    else
      return [_lookup(_.first(v), s)].concat(_lookup(_.rest(v), s))
  v

run = exports.run = (v, g) ->
  if _.isFunction(v)
    g = v
    v = null
  r = g(empty_subst)
  return r  if v is null
  r.map (s) ->
    _lookup v, s

